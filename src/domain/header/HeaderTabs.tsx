import React, { FC } from 'react'

import Tabs from '@material-ui/core/Tabs'
import Tab from '@material-ui/core/Tab'

import { useQuery } from '@apollo/react-hooks'
import { gql } from 'apollo-boost'

import { Link, Switch } from 'react-router-dom'

interface Project {
	id: string
	name: string
}

interface GetProjectData {
	projects: Project[]
}

const GET_PROJECTS = gql`
	{
		projects {
			id
			name
		}
	}
`
const HeaderTabs: FC = () => {
	const [value, setValue] = React.useState(0)
	const { loading, error, data } = useQuery<GetProjectData>(GET_PROJECTS)

	const handleChange = (
		event: React.ChangeEvent<{}>,
		newValue: number
	): void => {
		setValue(newValue)
	}

	const createTabs = (): JSX.Element | JSX.Element[] => {
		if (loading) return <Tab key={1} label="Loading..." />
		if (error) return <Tab key={1} label={`Error! ${error.message}`} />
		if (data && data.projects) {
			return data.projects.map((project?: any) => (
				<Tab
					key={project.id}
					label={project.name}
					to={`/projects/${project.id}`}
					component={Link}
				/>
			))
		}
		return <div>Unknown Error</div>
	}

	return (
		<Switch>
			<Tabs value={value} onChange={handleChange} aria-label="project tabs">
				{createTabs()}
			</Tabs>
		</Switch>
	)
}

export default HeaderTabs
