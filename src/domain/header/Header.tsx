import React, { FC } from 'react'

import { makeStyles, AppBar } from '@material-ui/core'

import { themeValues } from 'app/themes/themeValues'
import HeaderTabs from './HeaderTabs'

const useStyles = makeStyles({
	root: {
		height: themeValues().sizes.TabHeader.height,
		background: themeValues().gradients.background,
		position: 'relative',
		color: 'white',
		display: 'flex',
		alignItems: 'flex-end',
	},
})

const Header: FC = () => {
	const classes = useStyles()

	return (
		<div className={classes.root}>
			<AppBar position="static">
				<HeaderTabs />
			</AppBar>
		</div>
	)
}

export default Header
