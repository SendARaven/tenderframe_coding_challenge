import React, { FC } from 'react'

import { makeStyles, Theme, Typography } from '@material-ui/core'

const useStyles = makeStyles((theme: Theme) => ({
	root: {
		padding: theme.spacing(4),
	},
}))

const NotFound: FC = () => {
	const classes = useStyles()
	return (
		<div className={classes.root}>
			<Typography component="h1" variant="h2">
				404 Not Found
			</Typography>
		</div>
	)
}

export default NotFound
