import React, { FC } from 'react'
import { makeStyles, Theme, Typography } from '@material-ui/core'
import CustomButton from 'component/CustomButton'

const useStyles = makeStyles((theme: Theme) => ({
	root: {
		padding: theme.spacing(4),
	},
}))

const DataPage: FC = () => {
	const classes = useStyles()

	return (
		<div className={classes.root}>
			<Typography component="h1" variant="h3">
				Project
			</Typography>
			<CustomButton disabled>Button1</CustomButton>
			<CustomButton />
			<CustomButton />
		</div>
	)
}

export default DataPage
