import React, { FC } from 'react'
import { makeStyles, Theme, Typography, Paper } from '@material-ui/core'

const useStyles = makeStyles((theme: Theme) => ({
	root: {
		padding: theme.spacing(4),
	},
}))

const WelcomePage: FC = ({ children }) => {
	const classes = useStyles()
	return (
		<div className={classes.root}>
			<Typography component="h1" variant="h1">
				Tenderframe Coding Challenge!
			</Typography>
			<Paper className={classes.root}>
				<Typography variant="h5" component="h3">
					Please read and follow the instructions in README.md
				</Typography>
				<Typography component="p">
					Have fun! We are looking forward to your result.
				</Typography>
			</Paper>
			{children}
		</div>
	)
}

export default WelcomePage
