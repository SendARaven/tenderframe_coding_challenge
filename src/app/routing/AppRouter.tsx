import React, { ReactElement } from 'react'
import { Switch, BrowserRouter } from 'react-router-dom'
import { routes } from 'app/routing/routes'
import TemplateWelcome from '../../template/TemplateWelcome'
import WelcomePage from '../../domain/welcome/WelcomePage'
import DataPage from '../../domain/data/DataPage'
import NotFound from '../../domain/error/NotFound'

const AppRouter = (): ReactElement => {
	return (
		<BrowserRouter>
			<Switch>
				<TemplateWelcome exact path={routes.home} component={WelcomePage} />
				<TemplateWelcome exact path={routes.id} component={DataPage} />
				<TemplateWelcome component={NotFound} />
			</Switch>
		</BrowserRouter>
	)
}

export default AppRouter
