import React from 'react'
import clsx from 'clsx'
import { Button } from '@material-ui/core'
import { withStyles, WithStyles } from '@material-ui/core/styles'

const styles = {
	root: {
		background: 'red',
		borderRadius: 3,
		border: 0,
		color: 'white',
		height: 48,
		padding: '0 30px',
	},
	disabled: {},
	loading: {},
}
interface Props extends WithStyles<typeof styles> {
	children?: React.ReactNode
	className?: string
	disabled?: boolean
	loading?: boolean
}

const CustomButton = (props: Props): JSX.Element => {
	const { classes, children, className, ...other } = props

	return (
		<Button className={clsx(classes.root, className)} {...other}>
			{children || 'Button'}
		</Button>
	)
}

export default withStyles(styles)(CustomButton)
