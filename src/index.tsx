import React from 'react'
import ReactDOM from 'react-dom'

import ThemeProvider from '@material-ui/styles/ThemeProvider'
import { CssBaseline } from '@material-ui/core'

import theme from 'app/themes/baseMuiTheme'

import { ApolloProvider } from '@apollo/react-hooks'
import client from './app/services/apollo/Apollo'

import * as serviceWorker from './serviceWorker'
import AppRouter from './app/routing/AppRouter'

ReactDOM.render(
	<ApolloProvider client={client}>
		<ThemeProvider theme={theme}>
			<CssBaseline />
			<AppRouter />
		</ThemeProvider>
	</ApolloProvider>,
	document.getElementById('root')
)

serviceWorker.unregister()
