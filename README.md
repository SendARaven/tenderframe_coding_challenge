# Welcome to your coding challenge

Dear candidate,

you can proudly claim you reached level 2 in the hiring process of becoming a Tenderframe developer. That being said, we would like to see some convincing code in order to evaluate your programming skills. For that matter, we provide you with a boilerplate that reflects the tech-stack we're using. In addition to the below-mentioned tasks you can extend the app with components and features as you wish. Sky is the limit but don't invest more time than you are willing to.

The evaluation of the submitted result will be based on the following criteria:

- how is your work organized (file structure, components, commits (don't use one bulk commit at the end of the development, use meaningful commit messages for your growing codebase that show us that you work in a structured manner)
- how are you managing to work with a package manager (npm) and third-party packages
- how much are you committed to clean code and good-practice programming patterns

So, ready? Here are the tasks, finally.

# Tasks

1.  Have a detailed look at the folder structure and get an overview how we implemented the application in a domain-driven design. Take your time with this task as for the following work items we primarily value good structure and organization of your code.

2.  Add a logo to the top of the Drawer. How would you implement this? Would you prefer using a linked resource or a base64 decoded image? Please explain your decision here discussing pro's and con's:

    - Your answer: Im Falle eines Logos hinterlege ich eine Datei und würde nicht Base64-Codierung nutzen. Base64 kann zwar die Ladegeschwindigkeiten der Webseite bei kleineren Bildern verbessern, aber ist nicht sonderlich gut geeignet für SEO, da Google oder andere Suchmaschinen die Bilder nicht listet oder nur über Umwege. Im Falle eines Unternehmenslogos mit Wiedererkennungswert also meiner Meinung nach falsche Datensparsamkeit und sollte auf unwichtige Fremdsymbole wie zB. Socialmediaicons oder ähnliches angewendet werden.

3.  For communication with the backend we are using GraphQL. On the client side we use "Apollo Client" to create queries and mutations. In this coding challenge we use a package for a fake API. Get familiar with the package and adapt the schema in `schema.graphql` in order to provide project list and details fake data:

        type:
         - Project
            - id
            - name

        query:
         - project - get a project
         - projects - get all projects (3)

4.  With the data from 3. in the blue area at the top of the page render a tab for each project:

    - use the tab component from **Material UI**
    - each tab has the project name as label

5.  Implement a new url structure. The new url structure should contain a placeholder for the project id:

    - `/projects/<project_id>`
    - navigate with the created tabs
    - the active state of the tab should **depend on the url**

6.  Render some content in the white area for each tab

    - display the project name, as `h1` in the dom, but styled as `h3`
    - create 3 buttons with different variations. Consider composition!
    - each button has the properties:
      - loading
      - disabled
      - onClick callback
      - dynamic text
    - buttons vary in the color:
      - blue background
      - red background
      - green background

7.  create a simple 404 page in case the url is not matched by the router (no styling needed).

# How to start

Install all dependencies

- `run npm install`

Start the dev environment

- `npm start`

Start GQL API

- `./node_modules/.bin/graphql-faker --open`

In the browser

- http://localhost:3000
- http://localhost:9002/editor -> to create a GQL schema
- http://localhost:9002/graphql -> API endpoint

# Tipps

- be creative ;)
- when you see issues along the way: fix them
- use what the boilerplate provides and extend it!
- don't repeat yourself!
- use Composition!

# I can not commit ... what should i do?

We implemented a hook to run eslint before every commit to ensure the quality of the code.
You can configure your IDE to handle the linter errors or just run:

`npm run lint`

to see what makes problems. Warnings are ok, but errors not.

Happy coding :)
